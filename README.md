# 小梦想
##### 做Vue界最牛的模板

#### 项目基础要求
node：10.0.0及以上  
npm：最新的即可

#### 分支明细
master:项目基础版  
master-qiankun: Vite模块联邦 + qiankun微服务 (搭建中)

### 项目预览
![image](https://gitee.com/yssydudu/Vue3-Admin-Element-Qiankun-Template/raw/master/src/styles/img/preview_project.png)
	

#### Build Setup
``` bash

# 安装依赖
npm install

# 启动服务 localhost:5173
npm run dev
