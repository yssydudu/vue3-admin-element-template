import request from "./request";

export const get = (url, params) => {
  const config = {
    method: "get",
    url: url,
  };
  if (params) config.params = params;
  return request(config);
};

export const post = (url, params) => {
  const config = {
    method: "post",
    url: url,
  };
  if (params) config.data = params;
  return request(config);
};

export const put = (url, params) => {
  const config = {
    method: "put",
    url: url,
  };
  if (params) config.params = params;
  return request(config);
};

export const del = (url, params) => {
  const config = {
    method: "delete",
    url: url,
  };
  if (params) config.params = params;
  return request(config);
};
