// 所有接口挂在到index里面
const ctx = import.meta.globEager("./components/*.js")
let modules = {}
for (const key in ctx) {
   const keys = Object.keys(ctx[key])
   modules[keys] = ctx[key][keys]
}
export default modules