import axios from 'axios'
import { ElMessage } from 'element-plus'
const service = axios.create({
  baseURL: '/api'
})
// 请求拦截器
service.interceptors.request.use(config => {
   config.data = JSON.stringify(config.data); //数据转化,也可以使用qs转换
   config.headers = {
    "Content-Type": "application/json;charset=utf-8"
   }
  return config
}, error => {
  Promise.reject(error)
})

// 响应拦截器
service.interceptors.response.use(response => {
    switch (response.data.code) {
        case 200:
            return response.data;
        default:
            ElMessage.warning("系统悄悄罢工啦....")
            break
    }
}, (error) => {
  ElMessage.warning("系统悄悄罢工啦....")
  return Promise.resolve(error.response)
})
export default service