import { createApp } from "vue";
import App from "./App.vue";
// 路由
import router from "./router";
// 路由守卫
import "./permission";
// Pinia
import { createPinia } from "pinia";
// ElementPlus
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
// 共用CSS
import "@/styles/base.css";

// 动态背景 particles
import Particles from "particles.vue3";

// 注册共用api
import api from "./api/index";
const app = createApp(App)
  .use(router)
  .use(createPinia())
  .use(ElementPlus)
  .use(Particles);

app.config.globalProperties.$api = api;
// 引入ICON
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

// 引入微服务
import { registerMicroApps } from "qiankun";

// 接入微服务
registerMicroApps([
  {
    name: "flow",
    entry: "http://localhost:5174/",
    container: "#subApp",
    activeRule: "#/flow-graph",
    props:{
      router
    }
  },
]);
app.mount("#app");
