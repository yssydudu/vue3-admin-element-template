import router from "./router";
import { start, close } from "@/utils/nprogress.js";
import { getCookies } from "@/utils/cookie.js";
import { useStoreUser } from "@/pinia/modules/user.js";
import { setRouterPackag } from "@/utils/user.js";
const whiteList = ["/login"];
router.beforeEach(async (to, from, next) => {
  start();
  const token = getCookies("token");
  if (whiteList.includes(to.path)) {
    // 判断白名单页面 直接跳转
    next();
  } else if (!token) {
    // 判断有没有登录
    next("/login");
  } else {
    // 登录加载菜单
    const storeUser = useStoreUser();
    if (storeUser.menuList.length <= 0) {
      const routers = await storeUser.SetDynamicRoute();
      setRouterPackag(routers);
      next({ ...to, replace: true })
    } else {
      next();
    }
  }
});

router.afterEach(() => {
  close();
});

router.onError((err) => {
  close();
});
