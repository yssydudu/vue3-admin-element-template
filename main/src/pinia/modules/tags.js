import { defineStore } from "pinia";
// 顶部tags数据
export const useStoreTags = defineStore("tags", {
  state: () => {
    return {
      list: [],
    };
  },
  actions: {
    UpdateTags(list){
      this.list = list
    },
    // 添加
    PushTags(route) {
      return new Promise((resolve, reject) => {
        const index = this.list.findIndex((x) => x.fullPath === route.fullPath);
        if (index > -1) return;
        this.list.push(route);
        resolve(route);
      });
    },
    // 关闭单个
    CloseOneTags(route) {
      return new Promise((resolve) => {
        const index = this.list.findIndex((x) => x.fullPath === route.fullPath);
        this.list.splice(index, 1);
        resolve(this.list);
      });
    },
  },
});
