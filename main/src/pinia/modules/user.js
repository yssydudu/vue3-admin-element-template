import { defineStore } from "pinia";
import routers from '@/router/components/test.js'
export const useStoreUser = defineStore("user", {
  state: () => {
    return {
      // 菜单数据
      menuList: [],
    };
  },
  actions: {
    /* 获取用户菜单，添加动态路由 */
    SetDynamicRoute() {
      return new Promise((resolve, reject) => {
        try {
          this.menuList = routers;
          setTimeout(() => {
            resolve(routers);
          }, 1000);
        } catch (error) {
          reject(error);
        }
      });
    },
  },
});