export default [
  {
    path: "/",
    redirect: "/Home",
    component: "Layout",
    children: [
      {
        path: "/Home",
        component: "/Home/Index",
        meta: {
          title: "首页",
          icon: "Memo",
        },
      },
      {
        path: "/User",
        redirect: "/User/Index",
        meta: {
          title: "用户管理",
          icon: "UserFilled",
        },
        children: [
          {
            path: "/User/Index",
            name: "UserIndex",
            component: "/User/Index",
            meta: {
              title: "用户列表",
              icon: "Memo",
            },
          },
        ],
      },
      {
        path: "/Icon/Index",
        name: "IconIndex",
        component: "/Icon/Index",
        meta: {
          title: "图标管理",
          icon: "Memo",
        },
      },
      {
        path: "/flow-graph/Home",
        meta: {
          title: "乾坤子应用首页",
          icon: "Memo",
        },
      },
      {
        path: "/flow-graph/User",
        meta: {
          title: "乾坤子应用用户",
          icon: "Memo",
        },
      },
    ],
  }
];
