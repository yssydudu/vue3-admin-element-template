import Layout from '@/layout/Index.vue'
export default [
  {
    path: "/Login",
    name: "Login",
    component: () => import("@/views/Login/Index.vue"),
    meta: {
      hide:true
    }
  },
  {
    path: "/404",
    name: "404",
    component: () => import("@/views/404/Index.vue"),
    meta: {
      hide:true
    }
  },
  // {
  //   path: "/flow-graph/:chapters*",
  //   component: Layout
  // },
];
