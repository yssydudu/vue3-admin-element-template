import { createWebHistory,createWebHashHistory,createRouter } from "vue-router";

import whiteList from './components/whiteList'
const router = createRouter({
  history: createWebHashHistory(),
  routes:whiteList 
});

export default router;
