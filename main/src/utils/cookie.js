import Cookies from "js-cookie";
// 获取
export const getCookies = (key) => {
  if (!key) return null;
  try {
    return JSON.parse(Cookies.get(key))
  } catch (error) {
    return Cookies.get(key)
  }
};
// 存入
export const setCookies = (key, value, expires = 7) => {
  if (!key) return null;

  if(typeof value === 'object') {
    value = JSON.stringify(value);
  }
  return Cookies.set(key, value, { expires });
};
// 删除
export const removeCookies = (key) => {
  if (!key) return null;
  return Cookies.remove(key);
};
