import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
// 配置静态资源访问
import path from "path";
export default defineConfig(() => {
  return {
    port: 5173,
    base: "./",
    server: {
      proxy: {
        "/api": {
          target: "http://localhost:9999",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
      },
    },
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src"),
      },
    },
    //配置需要使用的插件列表
    plugins: [
      vue()
    ]
  };
});
