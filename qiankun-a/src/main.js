import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
// 乾坤
import {
  renderWithQiankun,
  qiankunWindow,
} from "vite-plugin-qiankun/dist/helper";
// ElementPlus
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
let app;
if (!qiankunWindow.__POWERED_BY_QIANKUN__) {
  createApp(App).use(router).use(ElementPlus).mount("#app");
} else {
  renderWithQiankun({
    mount(props) {
      app = createApp(App).use(router).use(ElementPlus);

      // 挂载全局父应用Router
      if (props.router) {
        app.config.globalProperties.parentRouter = props.router;
      }
      app.mount(
        props.container
          ? props.container.querySelector("#app")
          : document.getElementById("app")
      );
    },
    bootstrap() {
      console.log("--bootstrap");
    },
    update() {
      console.log("--update");
    },
    unmount() {
      console.log("--unmount");
      app?.unmount();
    },
  });
}
