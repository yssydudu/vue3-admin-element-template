import { createRouter, createWebHashHistory } from "vue-router";
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/flow-graph",
      redirect: '/flow-graph/Home',
      children: [
        {
          path: "/flow-graph/Home",
          name: "flow-graph/Home",
          component: () => import("../views/Home/Index.vue"),
        },
        {
          path: "/flow-graph/User",
          name: "flow-graph/User",
          component: () => import("../views/User/Index.vue"),
        }
      ]
    }

  ],
});

export default router;
