import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
// 乾坤
import qiankun from "vite-plugin-qiankun";
export default defineConfig({
  base: "http://localhost:5174/",
  server: {
    port: 5174,
    cors: true,
    origin: "http://localhost:5174",
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },
  plugins: [
    vue(),
    qiankun("flow", {
      useDevMode: true,
    }),
  ]
});
